from PIL import Image, ImageDraw, ImageFont
import cv2
import numpy as np
import random

def add_text_to_image(img, text): # Добавление текста на изображение
	font = cv2.FONT_HERSHEY_COMPLEX
	img = cv2.putText(img,text,(40, 40), font, 1,(255,255,255),2,cv2.LINE_AA)
	cv2.imshow("img",img)
	cv2.waitKey(0)

def get_img_attr(): # возвращает изображение, массив пикселей 
	im = Image.open("./LR3 - photo1.jpg")
	draw = ImageDraw.Draw(im)
	width, height = im.size
	pixels = np.array(im)

	return im, draw, width, height, pixels

def fragment1(): # Перевод в чернобелове изображение
	im, draw, width, height, pixels = get_img_attr()

	for i in range(height):
		for j in range(width):
			a = pixels[i, j][0]
			b = pixels[i, j][1]
			c = pixels[i, j][2]
			S = (int(a) + int(b) + int(c)) // 3
			if S > 255:
				S=255
			draw.point((j, i), (S, S, S))

	im.show()

def fragment2(): # эффект сепия
	im, draw, width, height, pixels = get_img_attr()
	depth = int(input('depth:'))

	for i in range(height):
		for j in range(width):
			a = pixels[i, j][0]
			b = pixels[i, j][1]
			c = pixels[i, j][2]
			S = (int(a) + int(b) + int(c)) // 3
			a = S + depth * 2
			b = S + depth
			c = S
			if a >255:
				a = 255
			if b >255:
				b = 255
			if c >255:
				c = 255
			draw.point((j, i), (a, b, c))
	im.show()

def fragment3(): # Эффект переэкспозиции
	im, draw, width, height, pixels = get_img_attr()

	for i in range(height):
		for j in range(width):
			a = pixels[i, j][0]
			b = pixels[i, j][1]
			c = pixels[i, j][2]
			draw.point((j, i), (255 - a, 255 - b, 255 - c))

	fnt = ImageFont.truetype("arial.ttf", 20)
	draw.text((10,10), "Эффект", font=fnt, fill=(255,255,255,255)) 
	im.show()

def fragment4(): # эффект зерна
	im, draw, width, height, pixels = get_img_attr()

	factor = int(input('factor:'))
	for i in range(height):
		for j in range(width):
			rand = random.randint(-factor, factor)
			a = pixels[i, j][0] + rand
			b = pixels[i, j][1] + rand
			c = pixels[i, j][2] + rand
			if a <0:
			    a = 0
			if b <0:
			    b = 0
			if c <0:
			    c = 0
			if a >255:
			    a = 255
			if b >255:
			    b = 255
			if c >255:
			    c = 255
			draw.point((j, i), (a, b, c))
	im.show()

def fragment5(): # Увеличение яркости
	im, draw, width, height, pixels = get_img_attr()
	factor = int(input('factor:'))

	for i in range(height):
		for j in range(width):
			a = pixels[i, j][0] + factor
			b = pixels[i, j][1] + factor
			c = pixels[i, j][2] + factor
			if a <0:
			    a = 0
			if b <0:
			    b = 0
			if c <0:
			    c = 0
			if a >255:
			    a = 255
			if b >255:
			    b = 255
			if c >255:
			    c = 255
			draw.point((j, i), (a, b, c))
	im.show()

def fragment6(): # монохромное изображение
	im, draw, width, height, pixels = get_img_attr()

	factor = int(input('factor:'))
	for i in range(height):
		for j in range(width):
			a = pixels[i, j][0]
			b = pixels[i, j][1]
			c = pixels[i, j][2]
			S = int(a) + int(b) + int(c)
			if S > (((255 + factor) // 2) * 3):
			    a, b, c = 255, 255, 255
			else:
			    a, b, c = 0, 0, 0
			draw.point((j, i), (a, b, c))
	im.show()

def fragment7():
	img = cv2.imread('./LR3 - photo1.jpg')
	kernel = np.ones((5,5),np.float32)/25
	dst = cv2.filter2D(img,-1,kernel)
	cv2.imshow('img',dst)
	cv2.waitKey(0)
	cv2.destroyAllWindows()



if __name__ == '__main__':
	# img1 = cv2.imread('./LR3 - photo1.jpg') # открываем изображения
	# img2 = cv2.imread('./LR3 - photo2.jpg')
	# img3 = cv2.imread('./LR3 - photo3.jpg')
	# img4 = cv2.imread('./LR3 - photo4.jpg')
	# for img in [img1, img2, img3, img4]:
	# 	add_text_to_image(img, 'Мой текст')
	# 	break
	fragment1()