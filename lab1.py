import sys

if __name__ == '__main__':
	print("Введите два операнда и номер операции. Пример 10 20 0")
	print("Доступны следующие операции: \n 0 - сложение \n 1 - вычитание \n 2 - умножение \n 3 - деление \n 4 - степень \n")
	input_ = input()
	try:
		splited_input = input_.split() # Ввод с консоли

		if len(splited_input) != 3: # Проверка ввода
			print("Неправильный формат ввода")
			sys.exit()

		first_op = float(splited_input[0])
		second_op = float(splited_input[1])
		op = int(splited_input[2])
		result = 0

		# Операции
		if op == 0:
			result = first_op + second_op
		if op == 1:
			result = first_op - second_op
		if op == 2:
			result = first_op * second_op
		if op == 3:
			result = first_op / second_op
		if op == 4:
			result = first_op ^ second_op
		if op > 4:
			print("Неправильная операция")

		print(result)

	except:
		print("Произошла ошибка")