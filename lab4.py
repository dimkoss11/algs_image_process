import cv2

def example1_1(): # подключение к веб-камере
	import cv2
	capture = cv2.VideoCapture(0)
	while(capture.isOpened()):
		ret, frame = capture.read()
		cv2.imshow('frame',frame)
		if cv2.waitKey(1) == ord('x'):
			break
	capture.release()
	cv2.destroyAllWindows()

def example1_2(): # использованием фильтров
	capture = cv2.VideoCapture(0)
	while(capture.isOpened()):
		ret, frame = capture.read()
		gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		cv2.imshow('gray',gray)
		cv2.imshow('gray1',gray)
		cv2.imshow('gray2',gray)
		cv2.imshow('gray3',gray)
		cv2.imshow('gray4',gray)
		cv2.imshow('gray5',gray)

		if cv2.waitKey(1) == ord('x'):
			break
	capture.release()
	cv2.destroyAllWindows() 

def example1_3(): # сохранение видео
	capture = cv2.VideoCapture(0)
	fourcc = cv2.VideoWriter_fourcc(*'XVID')
	out = cv2.VideoWriter('./output.avi',fourcc, 20.0, (640,480))

	while(capture.isOpened()):
		ret, frame = capture.read()
		cv2.imshow('frame',frame)
		out.write(frame)

		if cv2.waitKey(1) == ord('x'):
			break

	capture.release()
	out.release()
	cv2.destroyAllWindows() 


def example1_4(): # чтение видео
	cap = cv2.VideoCapture('./output.avi')
	while True:
		ret, frame = cap.read()
		if frame is None:
			break
		cv2.imshow('app',frame)
		if cv2.waitKey(1) == ord('x'):
			break
	cap.release()
	cv2.destroyAllWindows()

def example2_1(): # Числовой анализ видеопотока
	video = cv2.VideoCapture(0)
	a=0

	while True:
		a = a + 1 #miliseconds
		check, frame = video.read()
		print(frame)
		gray=cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		cv2.imshow('MyCam',gray)
		if cv2.waitKey(1) == ord('x'):
			break

	print(a)
	video.release()
	cv2.destroyAllWindows()

if __name__ == '__main__':
	example1_2()